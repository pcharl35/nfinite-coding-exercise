import './App.css'
import React from 'react'
import ValidateUploadForm from './ValidateUploadForm'


function App() {

  return (
    <div className="App">
      <ValidateUploadForm />
    </div>
  )
}

export default App
