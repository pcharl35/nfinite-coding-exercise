import React from 'react';
import axios from 'axios';


class ValidateUploadForm extends React.Component {
  constructor() {
    super();
    this.state = {
      csvData: '',
      data: [],
      errors: []
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(e) {
    this.setState({ csvData: e.target.files[0] });
  }

  submit() {
    const formData = new FormData();
    formData.append('csv', this.state.csvData);
    let url = 'http://localhost:5000/upload';

    axios
      .post(url, formData, {})
      .then(res => {
        this.setState({ csvData: '', data: res.data.data, errors: res.data.errors });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <>
        <label>Select a CSV file to validate: </label>
        <input type="file" accept=".csv" name="csv" id="csv" onChange={this.handleInputChange}></input>
        <button onClick={() => this.submit()}>Validate!</button>
        {
            this.state.data.length > 0 ? 
            <div>
                {this.state.data.map((item, index) => (
                    <p key={index}>Id: {item.id} Name: {item.name} Dimensions: {item.picture.width}X{item.picture.height}
                    <img src={item.picture.url} alt={this.state.errors?.find(err => err.id === item.id)?.message ?? ''}></img></p>
                ))}
            </div>
            : ''
        }
      </>
    );
  }
}

export default ValidateUploadForm;

/*{this.state.resp && this.state.resp.data.data ? <ValidationTable data={this.state.resp.data.data} /> : null}*/