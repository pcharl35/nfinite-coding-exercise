import express from 'express'
import cors from 'cors'

const getImageDimensions = require('./src/getImageDimensions')
const app = express()
const csv = require('fast-csv')
const multer = require('multer')
const upload = multer()
const validateEntry = require('./src/validateEntry')

app.use(cors())

app.get('/', (req, res) => {
  res.send('This is from express.js')
})

app.post('/upload', upload.single('csv'), (req, res) => {

  const uploadResponse = {
    data: [],
    errors: []
  }

  let promises = [];

  csv.parseString(req.file.buffer.toString(), { delimiter: ';', headers: true })
    .on('data', (data) => {

      let dataObj = {
        id: data['id'],
        name: data['name'],
        picture: {
          url: data['url'],
          width: 0,
          height: 0
        }
      }

      let promise = getImageDimensions(dataObj.picture.url).then((dimensions) => {
        dataObj.picture.width = dimensions.width
        dataObj.picture.height = dimensions.height
        uploadResponse.data.push({
          id: data['id'], name: data['name'],
          picture: { url: data['url'], width: dimensions.width, height: dimensions.height }
        })
      })
        .catch((error) => {
          uploadResponse.data.push({
            id: data['id'], name: data['name'],
            picture: { url: data['url'], width: 0, height: 0 }
          })
          uploadResponse.errors.push({ id: dataObj.id, message: error })
        }).finally(() => {
          let errorMsg = validateEntry(dataObj)
          if (errorMsg != '') {
            if (uploadResponse.errors.find((entry) => { entry.id == dataObj.id }) != undefined) {
              var errorObj = uploadResponse.errors.find((entry) => { entry.id == dataObj.id })
              errorObj.message += '; ' + errorMsg
            }
            else {
              uploadResponse.errors.push({ id: dataObj.id, message: errorMsg })
            }
          }
        })

      promises.push(promise);

    })
    .on('end', () => {
      Promise.all(promises).then(() => {
        res.send(JSON.stringify(uploadResponse))
      });
    });
})

const port = process.env.PORT || 5000
app.listen(port, () => {
  console.log(`server started on port ${port}: http://localhost:${port}`)
})
