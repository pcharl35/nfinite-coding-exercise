import Joi from 'joi'

const entrySchema = Joi.object({
    id: Joi.string().hex().required(),

    name: Joi.string().max(300).required(),

    picture: Joi.object({
      url: Joi.string().required(),

      width: Joi.number(),

      height: Joi.number()
    })
  })

function validateEntry(entry)
{
    let errorMsg = ''

    let validationResult = entrySchema.validate(entry)
    if(validationResult.error != undefined)
    {
        errorMsg = validationResult.error.message
    }
    return errorMsg
}


module.exports = validateEntry