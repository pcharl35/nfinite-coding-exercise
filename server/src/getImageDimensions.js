const https = require('https')
const sizeOf = require('image-size')

let width = 0
let height = 0
let errorMsg = ''

function getImageDimensions(url) {

  return new Promise((resolve, reject) => {
    https.get(url, (response) => 
    {
      const imgChunks = []
      response.on('data', (imgChunk) => {
        imgChunks.push(imgChunk)
      }).on('end', () => {
        try {
          const imgBuffer = Buffer.concat(imgChunks)
          const dimensions = sizeOf(imgBuffer)
          width = dimensions.width
          height = dimensions.height
          resolve({ width, height })
        }
        catch (e) {
          errorMsg = e.message
          reject(errorMsg)
        }
      })
    })
  })
}

module.exports = getImageDimensions